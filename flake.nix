{
  description = "AoC 2021";

  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "advent-2021";
      buildInputs = with pkgs; [
        racket
      ];
    };
  };
}
