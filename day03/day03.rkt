#lang racket

(require "../lib/lib.rkt")
(require rackunit)

(define
  GIVEN
  '("00100"
    "11110"
    "10110"
    "10111"
    "10101"
    "01111"
    "00111"
    "11100"
    "10000"
    "11001"
    "00010"
    "01010"))

(define (most-occurring lst)
  (define (most-occurring/acc zeroes ones therest)
    (cond
      [(empty? therest) (if (> zeroes ones) "0" "1")]
      [else
        (if (equal? (first therest) #\0)
        (most-occurring/acc (add1 zeroes) ones (rest therest))
        (most-occurring/acc zeroes (add1 ones) (rest therest)))]))
  (most-occurring/acc 0 0 lst))

(define (gamma-rate lst)
  (define lst-charlists (map string->list lst))
  (define transposed (apply map list lst-charlists))
  (define singletons (map most-occurring transposed))
  (define joined (foldr string-append "" singletons))
  joined)
(check-equal? (gamma-rate GIVEN) "10110")

(define (flip-bit chr)
  (cond
    [(equal? chr #\0) "1"]
    [(equal? chr #\1) "0"]))

(define (flip-bits str)
  (define (flip-bits-charlist charlist)
    (cond
      [(empty? charlist) ""]
      [else
        (string-append
          (flip-bit (first charlist))
          (flip-bits-charlist (rest charlist)))]))
  (flip-bits-charlist (string->list str)))

(check-equal? (flip-bits "") "")
(check-equal? (flip-bits "1010") "0101")
(check-equal? (flip-bits "11111") "00000")
(check-equal? (flip-bits "11011") "00100")

(define (binstring-to-decimal binstring)
  (string->number binstring 2))

(define (day03-p1 lst)
  (define gamma-str (gamma-rate lst))
  (define gamma (binstring-to-decimal gamma-str))
  (define epsilon (binstring-to-decimal (flip-bits gamma-str)))
  (* gamma epsilon))
(check-equal? (day03-p1 GIVEN) 198)

(with-line-list-str "input" day03-p1)
